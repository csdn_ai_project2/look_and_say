import tensorflow as tf
import numpy as np

from inference_web import *
from utils import vocabulary

from flask import Flask, request, render_template

from datetime import *
import random


FLAGS = tf.app.flags.FLAGS

tf.flags.DEFINE_string("vocab_file", "data/flickr8k/word_counts.txt", "Text file containing the vocabulary.")

tf.flags.DEFINE_string("vggpb_path", "data/vgg_19.pb",
                       "Model checkpoint file or directory containing a "
                       "model checkpoint file.")
tf.flags.DEFINE_string("graph_path", "data/train/freeze_model.pb",
                       "Model checkpoint file or directory containing a "
                       "model checkpoint file.")



def load_graph(pb_filename):
    with tf.gfile.GFile(pb_filename, "rb") as f:
         graph_def = tf.GraphDef()
         graph_def.ParseFromString(f.read())
    with tf.Graph().as_default() as graph:
         tf.import_graph_def(graph_def, name="")
    return graph 


vocab = vocabulary.Vocabulary(FLAGS.vocab_file)
lstm = LSTMDecoder(FLAGS.graph_path,vocab,max_caption_length=20)

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

app = Flask(__name__)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

def give_filename(filename):
    nowtime = datetime.now().strftime("%Y%m%d%H%M%S") 
    random_num = random.randint(0,100)  
    if random_num<10:
       rnum = str(random_num).zfill(2)
    else:
       rnum = str(random_num) 
    return str(nowtime) + str(rnum)

def inference(selected_filepath,output_img):
    graph = load_graph(FLAGS.vggpb_path)  
    x = graph.get_tensor_by_name('input:0') 
    y = graph.get_tensor_by_name('output:0')
    with tf.Session(graph=graph) as sess:
         with tf.gfile.FastGFile(selected_filepath, "rb") as f:
              image_data = f.read()
         y = tf.reshape(y, [-1, 196, 512])
         context = sess.run([y],feed_dict={x:image_data})
         tf.get_variable_scope().reuse_variables()
         feat = np.squeeze(context)
         caption, attention = lstm.decode(feat)
         image = load_image_into_numpy_array(selected_filepath)
         #lstm.show_caption(caption,image)
         lstm.show_attention(caption, attention, image, output_img)


@app.route('/', methods=['GET', 'POST'])
def upload():
    index_img = 'static/init.jpg'
    render_template('index.html', output_img=index_img)
    if request.method =='POST':
       file = request.files['file']
       if file and allowed_file(file.filename):
          file_name = give_filename(file.filename)
          selected_filepath = 'static/uploads/'+file_name+'0.jpg'
          output_img = 'static/uploads/'+file_name+'1.jpg'
          file.save(selected_filepath)
          inference(selected_filepath,output_img)   
          return render_template('index.html',output_img=output_img)
    return render_template('index.html',output_img=index_img)

if __name__ == "__main__":
    app.run(debug=False)
