import tensorflow as tf
import numpy as np

from inference import *
from utils import vocabulary



FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string("vocab_file", "data/flickr8k/word_counts.txt", "Text file containing the vocabulary.")

tf.flags.DEFINE_string("demo_method", "show", "demo model: show or show_attetion_focus")

tf.flags.DEFINE_string("vggpb_path", "data/vgg_19.pb",
                       "Model checkpoint file or directory containing a "
                       "model checkpoint file.")
tf.flags.DEFINE_string("graph_path", "data/train/freeze_model.pb",
                       "Model checkpoint file or directory containing a "
                       "model checkpoint file.")
tf.flags.DEFINE_string("input_files", "data/image/test3.jpg",
                       "File pattern or comma-separated list of file patterns "
                       "of image files.")



def load_graph(pb_filename):
    with tf.gfile.GFile(pb_filename, "rb") as f:
         graph_def = tf.GraphDef()
         graph_def.ParseFromString(f.read())
    with tf.Graph().as_default() as graph:
         tf.import_graph_def(graph_def, name="")
    return graph 


def main(_):

    graph = load_graph(FLAGS.vggpb_path)

    
    with tf.Session(graph=graph) as sess:
         x = graph.get_tensor_by_name('input:0') 
         y = graph.get_tensor_by_name('output:0')
         vocab = vocabulary.Vocabulary(FLAGS.vocab_file)
         lstm = LSTMDecoder(FLAGS.graph_path,vocab,max_caption_length=20)

         filenames = []
         for file_pattern in FLAGS.input_files.split(","):
             filenames.extend(tf.gfile.Glob(file_pattern))
    
         for filename in filenames:
        
             with tf.gfile.FastGFile(filename, "rb") as f:
                  image_data = f.read()
             y = tf.reshape(y, [-1, 196, 512])
             context = sess.run([y],feed_dict={x:image_data})
             tf.get_variable_scope().reuse_variables()
             feat = np.squeeze(context)

             caption, attention = lstm.decode(feat)
    
             #image = Image.open(FLAGS.input_files)
             image = load_image_into_numpy_array(filename)
             if FLAGS.demo_method == "show":
                lstm.show_caption(caption,image)
             else:
                lstm.show_attention(caption, attention, image, "./pic.jpg")
      
if __name__ == "__main__":
    tf.app.run()
