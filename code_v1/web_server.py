import tensorflow as tf
import numpy as np

from ops import image_embedding
from inference_web import *
from utils import vocabulary

from flask import Flask, request, render_template

from datetime import *
import random


FLAGS = tf.app.flags.FLAGS

tf.flags.DEFINE_string("vocab_file", "data/flickr8k/word_counts.txt", "Text file containing the vocabulary.")

tf.flags.DEFINE_string("checkpoint_path", "data/vgg_19.ckpt",
                       "Model checkpoint file or directory containing a "
                       "model checkpoint file.")
tf.flags.DEFINE_string("graph_path", "data/train/freeze_model.pb",
                       "Model checkpoint file or directory containing a "
                       "model checkpoint file.")

def process_image(encoded_image,
                  height=224,
                  width=224,
                  image_format="jpeg"):


    # Decode image into a float32 Tensor of shape [?, ?, 3] with values in [0, 1).
    with tf.name_scope("decode", values=[encoded_image]):
        if image_format == "jpeg":
           image = tf.image.decode_jpeg(encoded_image, channels=3)
        elif image_format == "png":
           image = tf.image.decode_png(encoded_image, channels=3)
        else:
           raise ValueError("Invalid image format: %s" % image_format)
    image = tf.image.convert_image_dtype(image, dtype=tf.float32)

    # Central crop, assuming resize_height > height, resize_width > width.
    image = tf.image.resize_image_with_crop_or_pad(image, height, width)

    return image


vocab = vocabulary.Vocabulary(FLAGS.vocab_file)
lstm = LSTMDecoder(FLAGS.graph_path,vocab,max_caption_length=20)

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

app = Flask(__name__)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

def give_filename(filename):
    nowtime = datetime.now().strftime("%Y%m%d%H%M%S") 
    random_num = random.randint(0,100)  
    if random_num<10:
       rnum = str(random_num).zfill(2)
    else:
       rnum = str(random_num) 
    return str(nowtime) + str(rnum)

def inference(selected_filepath,output_img):    
    image_feed = tf.placeholder(dtype=tf.string, shape=[], name="image_feed")
    images = tf.expand_dims(process_image(image_feed), 0)
    vgg_output = image_embedding.vgg_19_extract(
      images,
      trainable=False,
      is_training=False)
    vgg_variables = tf.get_collection(
      tf.GraphKeys.GLOBAL_VARIABLES, scope="vgg_19")
    saver = tf.train.Saver()
    with tf.Session() as sess:
         saver.restore(sess, FLAGS.checkpoint_path)
         with tf.gfile.FastGFile(selected_filepath, "rb") as f:
              image_data = f.read()
         context = tf.reshape(vgg_output, [-1, 196, 512])
         context = sess.run(context,feed_dict={image_feed:image_data})
         tf.get_variable_scope().reuse_variables()
         feat = np.squeeze(context)
         caption, attention = lstm.decode(feat)
         image = load_image_into_numpy_array(selected_filepath)
         #lstm.show_caption(caption,image)
         lstm.show_attention(caption, attention, image, output_img)


@app.route('/', methods=['GET', 'POST'])
def upload():
    index_img = 'static/init.jpg'
    render_template('index.html', output_img=index_img)
    if request.method =='POST':
       file = request.files['file']
       if file and allowed_file(file.filename):
          file_name = give_filename(file.filename)
          selected_filepath = 'static/uploads/'+file_name+'0.jpg'
          output_img = 'static/uploads/'+file_name+'1.jpg'
          file.save(selected_filepath)
          inference(selected_filepath,output_img)   
          return render_template('index.html',output_img=output_img)
    return render_template('index.html',output_img=index_img)

if __name__ == "__main__":
    app.run(debug=False)
